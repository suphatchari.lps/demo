﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exam1_2.aspx.cs" Inherits="Demo.Exam2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Developer Exam 1.2</title>
    <script src="Scripts/jquery-3.5.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Exam.css" rel="stylesheet" />
</head>

<body>
    <form id="form2" runat="server">
        <div style="font-size: medium;">
            <asp:HyperLink ID="HyperLinkBackFrom1_2" NavigateUrl="Main.aspx" runat="server"><< Main</asp:HyperLink>
        </div>

        <div style="margin-top:5px">
            <h2>1.2 Pattern on below</h2>
        </div>
        <div id="example" class="row" style="padding: 1px; margin: 1px;">
            <div class="col-sm-3">
                <h5>n = 1</h5>
                <p style="padding: 0px; margin: 0px">X</p>
            </div>
            <div class="col-sm-3">
                <h5>n = 2</h5>
                <p style="padding: 0px; margin: 0px">
                    XX<br>
                    XX
                </p>
            </div>
            <div class="col-sm-3">
                <h5>n = 4</h5>
                <p style="padding: 0px; margin: 0px">
                    X&ensp;&ensp;X<br>
                    XXXX<br>
                    XXXX<br>
                    X&ensp;&ensp;X
                </p>
            </div>
            <div class="col-sm-3">
                <h5>n=5</h5>
                <p style="padding: 0px; margin: 0px; margin-bottom:5px;">
                    X&ensp;&ensp;&ensp;X<br>
                    XX&ensp;XX<br>
                    XXXXX<br>
                    XX&ensp;XX<br>
                    X&ensp;&ensp;&ensp;X
                </p>
            </div>
        </div>
        <div>
            <asp:Label ID="Label2" runat="server" Text="Input 'n' : "></asp:Label>
            <asp:TextBox ID="TxtInputFor1_2" runat="server" TextMode="Number" ErrorMessage="Only Numbers allowed"></asp:TextBox>
            <asp:Button ID="bt2" runat="server" OnClick="Button2_click" Text="Click!" />
        </div>
        <div class="row" style="padding: 5px; margin: 5px;">
            <div class="col-sm-12" style="text-align: center; border-style: none; font-size: medium; font-family: Tahoma;">
                <h5 ><asp:Label ID="IntInput2" runat="server" style="padding: 0px; margin: 0px"></asp:Label></h5>
                <p style="padding: 0px; margin: 0px; margin-bottom:5px">
                    <asp:Label ID="Result2" runat="server" style="padding: 0px; margin: 0px"></asp:Label>
                </p>
            </div>
        </div>

    </form>
</body>
</html>
