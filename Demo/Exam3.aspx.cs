﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;

namespace Demo
{
    public partial class Exam3 : System.Web.UI.Page
    {
        List<User> userList = new List<User>();
        protected void Page_Load(object sender, EventArgs e)
        {
            setUpData();
        }

        private void setUpData()
        {
            string jsonStr = (new WebClient()).DownloadString("https://jsonplaceholder.typicode.com/users");
            JavaScriptSerializer jsObject = new JavaScriptSerializer();
            userList = new List<User>();
            userList = jsObject.Deserialize<List<User>>(jsonStr);
            UserGridView.DataSource = userList;
            UserGridView.DataBind();
        }

        protected void Button_Search(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(TxtInputFor3.Text))
            {
                if (userList == null)
                {
                    setUpData();
                }
                string strKeyword = TxtInputFor3.Text.Trim();
                List<User> searchingList = new List<User>();
                foreach (User user in userList)
                {
                    if( user.name.Contains(strKeyword) || user.username.Contains(strKeyword) || 
                        user.email.Contains(strKeyword) || user.address.street.Contains(strKeyword) || 
                        user.address.suite.Contains(strKeyword) || user.address.city.Contains(strKeyword) ||
                        user.address.zipcode.Contains(strKeyword) || user.address.geo.lat.Contains(strKeyword) ||
                        user.address.geo.lng.Contains(strKeyword) || user.phone.Contains(strKeyword) ||
                        user.website.Contains(strKeyword) || user.company.name.Contains(strKeyword) ||
                        user.company.catchPhrase.Contains(strKeyword) || user.company.bs.Contains(strKeyword) )
                    {
                        searchingList.Add(user);
                    }
                }
                UserGridView.DataSource = searchingList;
                UserGridView.DataBind();
                result.Text = "Total : " + searchingList.Count() + " row(s)";
            }
        }

        protected void Button_Clear(object sender, EventArgs e)
        {
            TxtInputFor3.Text = "";
            result.Text = "";
            setUpData();
        }
    }
}