﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo
{
    public partial class Exam21 : System.Web.UI.Page
    {
        String errorFormatStr = "<span style=\"color: red\">Maybe input string was not in a correct format! \"day/month/year\"</span>";
        String errorValueStr = "<span style=\"color: red\">Value can not be null, and day = 1-31, month = 1-12, year = 1-18446744073709551615</span>";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button_Clear(object sender, EventArgs e)
        {
            TxtInputFor2.Text = "";
            Result2.Text = "";
        }

        protected void Button_Cal(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(TxtInputFor2.Text))
            {
                string[] dateStr = TxtInputFor2.Text.Split('/');
                if (dateStr.Length == 3)
                {
                    try
                    {
                        int getDay = Int32.Parse(dateStr[0]);
                        int getMonth = Int32.Parse(dateStr[1]);
                        UInt64 getYear = UInt64.Parse(dateStr[2]);
                        if (getDay > 0 && getDay <= 31 && getMonth > 0 && getMonth <= 12 && getYear > 0)
                        {
                            string lastTwoDigitYearStr = "";
                            int lastTwoDigitYear = 0;
                            if (dateStr[2].Length > 2)
                            {
                                lastTwoDigitYearStr = dateStr[2].Substring(dateStr[2].Length - 2);
                            }
                            else
                            {
                                lastTwoDigitYearStr = dateStr[2];
                            }
                             lastTwoDigitYear = Int32.Parse(lastTwoDigitYearStr);
                            resultLabel.Text = "Result";
                            Result2.Text =
                                //"day: " + getDay +
                                //"<br>month: " + getMonth +
                                //"<br>year: " + getYear +
                                //"<br>lastTwoDigitYear: " + lastTwoDigitYear + "<br>" + 
                                calculateDate(getDay, getMonth, getYear, lastTwoDigitYear) ;
                        }
                        else
                        {
                            resultLabel.Text = "";
                            Result2.Text = "1 " + errorValueStr;
                        }
                    }
                    catch (Exception)
                    {
                        resultLabel.Text = "";
                        Result2.Text = "2 " + errorValueStr;
                    }
                }
                else
                {
                    resultLabel.Text = "";
                    Result2.Text = errorFormatStr;
                }
            }
        }

        private string calculateDate(int day, int month, UInt64 year, int lastTwoDigitYear)
        {
            string resultStr = "";
            int section1_Ans = 0;
            int section2_Ans = 0;
            int section3_Ans = 0;
            int section4_Ans = 0;
            int section5_Ans = 0;
            int sum = 0;
            int dayInWeek = 0;
            if (lastTwoDigitYear >= 12)
            {
                section1_Ans = CalculateSection1(day, month, year);
                section2_Ans = CalculateSection2(year);
                section3_Ans = CalculateSection3(lastTwoDigitYear, 1);
                section4_Ans = CalculateSection4(lastTwoDigitYear);
                section5_Ans = CalculateSection5(section4_Ans);
                sum = (section1_Ans + section2_Ans + section3_Ans + section4_Ans + section5_Ans);
                dayInWeek = CheckDayInWeek(sum);
                resultStr = finalFormatResult(dayInWeek, day, month, year);
            }
            else if (lastTwoDigitYear >= 8 && lastTwoDigitYear < 12)
            {
                section1_Ans = CalculateSection1(day, month, year);
                section2_Ans = CalculateSection2(year);
                section3_Ans = CalculateSection3(lastTwoDigitYear, 2);
                sum = (section1_Ans + section2_Ans + section3_Ans);
                dayInWeek = CheckDayInWeek(sum);
                resultStr = finalFormatResult(dayInWeek, day, month, year);
            }
            else if (lastTwoDigitYear >= 4 && lastTwoDigitYear < 8)
            {
                section1_Ans = CalculateSection1(day, month, year);
                section2_Ans = CalculateSection2(year);
                section3_Ans = CalculateSection3(lastTwoDigitYear, 3);
                sum = (section1_Ans + section2_Ans + section3_Ans);
                dayInWeek = CheckDayInWeek(sum);
                resultStr = finalFormatResult(dayInWeek, day, month, year);
            }
            else if (lastTwoDigitYear >= 0 && lastTwoDigitYear < 4)
            {
                section1_Ans = CalculateSection1(day, month, year);
                section2_Ans = CalculateSection2(year);
                section3_Ans = CalculateSection3(lastTwoDigitYear, 4);
                sum = (section1_Ans + section2_Ans + section3_Ans);
                dayInWeek = CheckDayInWeek(sum);
                resultStr = finalFormatResult(dayInWeek, day, month, year);
            }
            return resultStr;
        }

        private int CalculateSection1 (int day, int month, UInt64 year)
        {
            return day - CheckMonthValueForCalculate(month, year);
        }

        private int CalculateSection2(UInt64 year)
        {
            UInt64 modYear = year % 400;
            if (modYear >= 0 && modYear <= 99)
            {
                return 2;
            }
            else if (modYear >= 100 && modYear <= 199)
            {
                return 7;
            }
            else if (modYear >= 200 && modYear <= 299)
            {
                return 5;
            }
            else
            {
                return 3;
            }
        }

        private int CalculateSection3(int lastTwoDigitYear, int conditionNum)
        {
            if (conditionNum == 1)
            {
                return lastTwoDigitYear / 12;
            }
            else if (conditionNum == 2)
            {
                return lastTwoDigitYear - 12;
            }
            else if (conditionNum == 3)
            {
                return lastTwoDigitYear - 13;
            }
            else
            {
                return lastTwoDigitYear - 14;
            }
        }

        private int CalculateSection4(int lastTwoDigitYear)
        {
            return lastTwoDigitYear % 12;
        }

        private int CalculateSection5(int section4_Ans)
        {
            return section4_Ans / 4;
        }

        private int CheckMonthValueForCalculate(int month, UInt64 year)
        {
            if(month == 1)
            {
                if (year % 4 != 0)
                {
                    return 3;
                }
                else
                {
                    return 4;
                }
            }
            else if (month == 2)
            {
                if (year % 4 != 0)
                {
                    return 7;
                }
                else
                {
                    return 1;
                }
            }
            else if (month == 3 || month == 11)
            {
                return 7;
            }
            else if (month == 4)
            {
                return 4;
            }
            else if (month == 5)
            {
                return 9;
            }
            else if (month == 6)
            {
                return 6;
            }
            else if (month == 7)
            {
                return 11;
            }
            else if (month == 8)
            {
                return 8;
            }
            else if (month == 9)
            {
                return 5;
            }
            else if (month == 10)
            {
                return 10;
            }
            else
            {
                return 12;
            }
        }
    
        private int CheckDayInWeek(int sum)
        {
            if (sum < 0)
            {
                return (int)(Math.Ceiling(((double)Math.Abs(sum)) / 7) * 7 + sum);
            }
            else
            {
                return (sum % 7);
            }
        }

        private string finalFormatResult(int dayInWeek, int day, int month, UInt64 year)
        {
            string[] dayInWeekArr = { "วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัสบดี", "วันศุกร์", "วันเสาร์" };
            string[] monthArr = { "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" };
            return dayInWeekArr[dayInWeek] + "ที่ " + day + " " + monthArr[month - 1] + " " + (year + 543);
        }
    }
}