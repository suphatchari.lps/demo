﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="Demo.Main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Developer Exam</title>
    <script src="Scripts/jquery-3.5.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Exam.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin-top:5px">
            <h1>Main Page</h1>
        </div>
        <div style="font-size: medium; margin-top:10px">
            1. Develop an application for given input data and output as following.
        </div>
        <div style="font-size: medium; padding-left: 50px">
            <asp:HyperLink ID="HyperLink1_1" NavigateUrl="Exam1_1.aspx" runat="server">>> 1.1</asp:HyperLink>
        </div>
        <div style="font-size: medium; padding-left: 50px">
            <asp:HyperLink ID="HyperLink1_2" NavigateUrl="Exam1_2.aspx" runat="server">>> 1.2</asp:HyperLink>
        </div>
        <div style="font-size: medium; padding-left: 50px">
            <asp:HyperLink ID="HyperLink1_3" NavigateUrl="Exam1_3.aspx" runat="server">>> 1.3</asp:HyperLink>
        </div>

        <div style="font-size: medium; margin-top:10px">
            2. Develop an application for date input (day/month/year) without using Library and Built-in Function (i.e. moment() and new Date() are not allowed).
        </div>
        <div style="font-size: medium; padding-left: 50px">
            <asp:HyperLink ID="HyperLink2" NavigateUrl="Exam2.aspx" runat="server">>> 2</asp:HyperLink>
        </div>

        <div style="font-size: medium; margin-top:10px">
            3. Create a page for display and searching users by using API from “https://jsonplaceholder.typicode.com/users” Response data must be in JSON format only.
        </div>
        <div style="font-size: medium; padding-left: 50px">
            <asp:HyperLink ID="HyperLink3" NavigateUrl="Exam3.aspx" runat="server">>> 3</asp:HyperLink>
        </div>
    </form>
</body>
</html>
