﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo
{
    public partial class demo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(TxtInputFor1_1.Text))
            {
                int inputInt = Int32.Parse(TxtInputFor1_1.Text);
                int size = 0;
                if(inputInt > 0)
                {
                    size = ((inputInt - 1) * 2) + 1;
                }
                string outputStr = "";
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        if (j < i && i <= Math.Floor(((double)size) / 2)) // Top-Left (0,0)
                        {
                            outputStr += "O";
                        }
                        else if (j > i && i < Math.Floor(((double)size) / 2) && j > (size - 1 - i)) // Top-Right (0,1)
                        {
                            outputStr += "O";
                        }
                        else if (j < i && i > Math.Floor(((double)size) / 2) && j < (size - 1 - i)) // Buttom-Left (1,0)
                        {
                            outputStr += "O";
                        }
                        else if (j > i && i >= Math.Floor(((double)size) / 2)) // Buttom-Right (1,1)
                        {
                            outputStr += "O";
                        }
                        else
                        {
                            if (i % 2 == 0)
                            {
                                if (j % 2 == 0)
                                {
                                    outputStr += "X";
                                }
                                else
                                {
                                    outputStr += "O";
                                }
                            }
                            else
                            {
                                if (j % 2 == 0)
                                {
                                    outputStr += "O";
                                }
                                else
                                {
                                    outputStr += "X";
                                }
                            }
                        }
                    }
                    if ((i + 1) < size)
                    {
                        outputStr += "<br>";
                    }
                }
                if (inputInt > 0)
                {
                    IntInput1.Text = "n = " + TxtInputFor1_1.Text;
                    Result1.Text = outputStr;
                }
                else
                {
                    IntInput1.Text = "";
                    Result1.Text = "";
                }
            }
            else
            {
                IntInput1.Text = "";
                Result1.Text = "";
            }
        }
    }
}