﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exam1_1.aspx.cs" Inherits="Demo.demo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Developer Exam 1.1</title>
    <script src="Scripts/jquery-3.5.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Exam.css" rel="stylesheet" />
</head>

<body>
    <form id="form1" runat="server">
        <div style="font-size: medium;">
            <asp:HyperLink ID="HyperLinkBackFrom1_1" NavigateUrl="Main.aspx" runat="server"><< Main</asp:HyperLink>
        </div>
        <div style="margin-top:5px">
            <h2>1.1 Pattern on below</h2>
        </div>
        <div id="example" class="row" style="padding: 1px; margin: 1px;">
            <div class="col-sm-3">
                <h5>n = 1</h5>
                <p style="padding: 0px; margin: 0px;">X</p>
            </div>
            <div class="col-sm-3">
                <h5>n = 2</h5>
                <p style="padding: 0px; margin: 0px">
                    XOX<br>
                    OXO<br>
                    XOX
                </p>
            </div>
            <div class="col-sm-3">
                <h5>n = 3</h5>
                <p style="padding: 0px; margin: 0px">
                    XOXOX<br>
                    OXOXO<br>
                    OOXOO<br>
                    OXOXO<br>
                    XOXOX
                </p>
            </div>
            <div class="col-sm-3">
                <h5>n = 4</h5>
                <p style="padding: 0px; margin: 0px; margin-bottom:5px">
                    XOXOXOX<br>
                    OXOXOXO<br>
                    OOXOXOO<br>
                    OOOXOOO<br>
                    OOXOXOO<br>
                    OXOXOXO
                </p>
            </div>
        </div>
        <div>
            <asp:Label ID="Label2" runat="server" Text="Input 'n' : "></asp:Label>
            <asp:TextBox ID="TxtInputFor1_1" runat="server" TextMode="Number" ErrorMessage="Only Numbers allowed"></asp:TextBox>
            <asp:Button ID="bt1" runat="server" OnClick="Button1_click" Text="Click!" />
        </div>
        <div class="row" style="padding: 5px; margin: 5px;">
            <div class="col-sm-12" style="text-align: center; border-style: none; font-size: medium; font-family: Tahoma;">
                <h5 ><asp:Label ID="IntInput1" runat="server" style="padding: 0px; margin: 0px"></asp:Label></h5>
                <p style="padding: 0px; margin: 0px; margin-bottom:5px">
                    <asp:Label ID="Result1" runat="server" style="padding: 0px; margin: 0px"></asp:Label>
                </p>
            </div>
        </div>

    </form>
</body>
</html>
