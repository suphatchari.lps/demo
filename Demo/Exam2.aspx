﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exam2.aspx.cs" Inherits="Demo.Exam21" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Developer Exam 2</title>
    <script src="Scripts/jquery-3.5.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Exam.css" rel="stylesheet" />
</head>

<body>
    <form id="form1" runat="server">
        <div style="font-size: medium;">
            <asp:HyperLink ID="HyperLinkBackFrom2" NavigateUrl="Main.aspx" runat="server"><< Main</asp:HyperLink>
        </div>
        
        <div style="margin-top:5px">
            <h2>2. Develop an application for date input (day/month/year)</h2>
        </div>

        <div style="margin-bottom:5px">
            <asp:Label ID="Label2" runat="server" Text="Input : "></asp:Label>
            <asp:TextBox ID="TxtInputFor2" runat="server" TextMode="Search"></asp:TextBox>
            <asp:Button ID="bt22" runat="server" OnClick="Button_Cal" Text="Calculate!"/>
            <asp:Button ID="clearBt22" runat="server" OnClick="Button_Clear" Text="Clear" style="margin-left:20px"/>
        </div>

        <div class="row" style="padding: 5px; margin: 5px;">
            <div class="col-sm-12" style="text-align: center; border-style: none; font-size: large; font-family: Tahoma;">
                <h5 ><asp:Label ID="resultLabel" runat="server" style="padding: 0px; margin: 0px"></asp:Label></h5>
                <p style="padding: 0px; margin: 0px; margin-bottom:5px">
                    <asp:Label ID="Result2" runat="server" style="padding: 0px; margin: 0px"></asp:Label>
                </p>
            </div>
        </div>

    </form>
</body>
</html>
