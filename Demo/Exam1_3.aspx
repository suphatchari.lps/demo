﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exam1_3.aspx.cs" Inherits="Demo.Exam1_3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Developer Exam 1.3</title>
    <script src="Scripts/jquery-3.5.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Exam.css" rel="stylesheet" />
</head>

<body>
    <form id="form3" runat="server">
        <div style="font-size: medium;">
            <asp:HyperLink ID="HyperLinkBackFrom1_3" NavigateUrl="Main.aspx" runat="server"><< Main</asp:HyperLink>
        </div>

        <div style="margin-top:5px">
            <h2>1.3 Pattern on below</h2>
        </div>
        <div id="example" class="row" style="padding: 1px; margin: 1px;">
            <div class="col-sm-2">
                <h5>n = 1</h5>
                <p style="padding: 0px; margin: 0px">X</p>
            </div>
            <div class="col-sm-2">
                <h5>n = 2</h5>
                <p style="padding: 0px; margin: 0px">
                    XX<br>
                    XX
                </p>
            </div>
            <div class="col-sm-2">
                <h5>n = 3</h5>
                <p style="padding: 0px; margin: 0px">
                    XXX<br>
                    <span style="color: red">YY</span>X<br>
                    XXX
                </p>
            </div>
            <div class="col-sm-2">
                <h5>n = 5</h5>
                <p style="padding: 0px; margin: 0px">
                    XXXXX<br>
                    <span style="color: red">YYYY</span>X<br>
                    XXX<span style="color: red">Y</span>X<br>
                    X<span style="color: red">YYY</span>X<br>
                    XXXXX
                </p>
            </div>
            <div class="col-sm-2">
                <h5>n = 6</h5>
                <p style="padding: 0px; margin: 0px">
                    XXXXXX<br>
                    <span style="color: red">YYYYY</span>X<br>
                    XXXX<span style="color: red">Y</span>X<br>
                    X<span style="color: red">Y</span>XX<span style="color: red">Y</span>X<br>
                    X<span style="color: red">YYYY</span>X<br>
                    XXXXXX
                </p>
            </div>
            <div class="col-sm-2">
                <h5>n = 7</h5>
                <p style="padding: 0px; margin: 0px; margin-bottom:5px;">
                    XXXXXXX<br>
                    <span style="color: red">YYYYYY</span>X<br>
                    XXXXX<span style="color: red">Y</span>X<br>
                    X<span style="color: red">YYY</span>X<span style="color: red">Y</span>X<br>
                    X<span style="color: red">Y</span>XXX<span style="color: red">Y</span>X<br>
                    X<span style="color: red">YYYYY</span>X<br>
                    XXXXXXX
                </p>
            </div>
        </div>
        <div>
            <asp:Label ID="Label2" runat="server" Text="Input 'n' : "></asp:Label>
            <asp:TextBox ID="TxtInputFor1_3" runat="server" TextMode="Number" ErrorMessage="Only Numbers allowed"></asp:TextBox>
            <asp:Button ID="bt3" runat="server" OnClick="Button3_click" Text="Click!" />
        </div>
        <div class="row" style="padding: 5px; margin: 5px;">
            <div class="col-sm-12" style="text-align: center; border-style: none; font-size: medium; font-family: Tahoma;">
                <h5 ><asp:Label ID="IntInput3" runat="server" style="padding: 0px; margin: 0px"></asp:Label></h5>
                <p style="padding: 0px; margin: 0px; margin-bottom:5px">
                    <asp:Label ID="Result3" runat="server" style="padding: 0px; margin: 0px"></asp:Label>
                </p>
            </div>
        </div>

    </form>
</body>
</html>
