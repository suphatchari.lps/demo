﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo
{
    public partial class Exam1_3 : System.Web.UI.Page
    {
        int[,] arr;
        int size = 0;
        int countRight = 0;
        int countDown = 0;
        int countLeft = 0;
        int countUp = 0;
        bool isContunue = false;
        //string debug;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button3_click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(TxtInputFor1_3.Text))
            {
                int inputInt = Int32.Parse(TxtInputFor1_3.Text);
                if (inputInt > 0)
                {
                    size = inputInt;
                    arr = new int[size, size];

                    string outputStr = "";

                    RecursiveLetters(1, 0);

                    for (int i = 0; i < arr.GetLength(0); i++)
                    {
                        for (int j = 0; j < arr.GetLength(1); j++)
                        {
                            if (arr[i, j] == 1)
                            {
                                outputStr += "<span style=\"color: red\">Y</span>";
                            }
                            else
                            {
                                outputStr += "X";
                            }
                        }
                        if (i + 1 < arr.GetLength(0))
                        {
                            outputStr += "<br>";
                        }
                    }

                    if (inputInt > 0)
                    {
                        IntInput3.Text = "n = " + TxtInputFor1_3.Text;
                        Result3.Text = outputStr + "<br>";
                    }
                    else
                    {
                        IntInput3.Text = "";
                        Result3.Text = "";
                    }
                }
            }
            else
            {
                IntInput3.Text = "";
                Result3.Text = "";
            }
        }
        private void RecursiveLetters(int i, int j)
        {
            countRight = 0;
            countDown = 0;
            countLeft = 0;
            countUp = 0;
            MoveRight(i, j);
        }

        private void MoveRight(int i, int j)
        {
            int indexMax = size - 1;
            int center = 0;
            if (indexMax % 2 == 0)
            {
                center = indexMax / 2;
            }
            if (i <= indexMax && j <= indexMax && i >= 0 && j >= 0)
            {
                int newI = i + 1;
                int newJ = j - 1;
                if (j < (indexMax - (countDown * 2)) && i < (indexMax - (countLeft * 2)))
                {
                    arr[i, j] = 1;
                    isContunue = true;
                    //debug += "MoveRight = [" + i + "," + j + "], Right = " + countRight + ", Down = " + countDown + ", Left = " + countLeft + ", Up = " + countUp + "<br>";
                    MoveRight(i, j + 1);
                }
                else if (isContunue && (j >= size / 2) && (i != center && j != center) && newI <= indexMax && newJ <= indexMax && newI >= 0 && newJ >= 0)
                {
                    //debug += "OUT MoveRight = [" + newI + "," + newJ + "]<br>";
                    countRight += 1;
                    isContunue = false;
                    MoveDown(newI, newJ);
                }
                //debug += "END MoveRight 2 = [" + i + "," + j + "]<br>";
            }
            //debug += "END MoveRight 1 = [" + i + "," + j + "]<br>";
        }

        private void MoveDown(int i, int j)
        {
            int indexMax = size - 1;
            int center = 0;
            if (indexMax % 2 == 0)
            {
                center = indexMax / 2;
            }
            if (i <= indexMax && j <= indexMax && i >= 0 && j >= 0)
            {
                int newI = i - 1;
                int newJ = j - 1;
                if (i < (indexMax - (countLeft * 2)) && j > (1 + (countUp * 2)))
                {
                    arr[i, j] = 1;
                    isContunue = true;
                    //debug += "MoveDown = [" + i + "," + j + "], Right = " + countRight + ", Down = " + countDown + ", Left = " + countLeft + ", Up = " + countUp + "<br>";
                    MoveDown(i + 1, j);
                }
                else if (isContunue && (i >= size / 2) && (i != center || j != center) && newI <= indexMax && newJ <= indexMax && newI >= 0 && newJ >= 0)
                {
                    //debug += "OUT MoveDown = [" + newI + "," + newJ + "]<br>";
                    countDown += 1;
                    isContunue = false;
                    MoveLeft(newI, newJ);
                }
                //debug += "END MoveDown 2 = [" + i + "," + j + "]<br>";
            }
            //debug += "END MoveDown 1 = [" + i + "," + j + "]<br>";
        }

        private void MoveLeft(int i, int j)
        {
            int indexMax = size - 1;
            int center = 0;
            if (indexMax % 2 == 0)
            {
                center = indexMax / 2;
            }
            if (i <= indexMax && j <= indexMax && i >= 0 && j >= 0)
            {
                int newI = i - 1;
                int newJ = j + 1;
                if (j >= 1 + (countUp * 2) && i > (countRight * 2))
                {
                    arr[i, j] = 1;
                    isContunue = true;
                    //debug += "MoveLeft = [" + i + "," + j + "], Right = " + countRight + ", Down = " + countDown + ", Left = " + countLeft + ", Up = " + countUp + "<br>";
                    MoveLeft(i, j - 1);
                }
                else if (isContunue && (j < size / 2) && (i != center || j != center) && newI <= indexMax && newJ <= indexMax && newI >= 0 && newJ >= 0)
                {
                    //debug += "OUT MoveLeft = [" + newI + "," + newJ + "]<br>";
                    countLeft += 1;
                    isContunue = false;
                    MoveUp(newI, newJ);
                }
                //debug += "END MoveLeft 2 = [" + i + "," + j + "]<br>";
            }
            //debug += "END MoveLeft 1 = [" + i + "," + j + "]<br>";
        }

        private void MoveUp(int i, int j)
        {
            int indexMax = size - 1;
            int center = 0;
            if (indexMax % 2 == 0)
            {
                center = indexMax / 2;
            }
            if (i <= indexMax && j <= indexMax && i >= 0 && j >= 0)
            {
                int newI = i + 1;
                int newJ = j;
                if (i >= 1 + (countRight * 2) && j < 1 + (countDown * 2))
                {
                    arr[i, j] = 1;
                    isContunue = true;
                    //debug += "MoveUp = [" + i + "," + j + "], Right = " + countRight + ", Down = " + countDown + ", Left = " + countLeft + ", Up = " + countUp + "<br>";
                    MoveUp(i - 1, j);
                }
                else if (isContunue && (i < size / 2) && (i != center || j != center) && newI <= indexMax && newJ <= indexMax && newI >= 0 && newJ >= 0)
                {
                    //debug += "OUT MoveUp = [" + newI + "," + j + "]<br>";
                    countUp += 1;
                    isContunue = false;
                    MoveRight(newI, newJ);
                }
                
            }
            //debug += "END MoveUp 1 = [" + i + "," + j + "]<br>";
        }
    }
}