﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo
{
    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public Address address { get; set; }
        public string phone { get; set; }
        public string website { get; set; }
        public Company company { get; set; }

        public User()
        {

        }

        //public User(int id, string name, string username, string email, Address address, string phone, string website, Company company)
        //{
        //    this.id = id;
        //    this.name = name;
        //    this.username = username;
        //    this.email = email;
        //    this.address = address;
        //    this.phone = phone;
        //    this.website = website;
        //    this.company = company;
        //}
    }

    public class Address
    {
        public string street { get; set; }
        public string suite { get; set; }
        public string city { get; set; }
        public string zipcode { get; set; }
        public Geo geo { get; set; }

        //public Address(string street, string suite, string city, string zipcode, Geo geo)
        //{
        //    this.street = street;
        //    this.suite = suite;
        //    this.city = city;
        //    this.zipcode = zipcode;
        //    this.geo = geo;
        //}
    }

    public class Geo
    {
        public string lat { get; set; }
        public string lng { get; set; }

        //public Geo(string lat, string lng)
        //{
        //    this.lat = lat;
        //    this.lng = lng;
        //}
    }

    public class Company
    {
        public string name { get; set; }
        public string catchPhrase { get; set; }
        public string bs { get; set; }

        //public Company(string name, string catchPhrase, string bs)
        //{
        //    this.name = name;
        //    this.catchPhrase = catchPhrase;
        //    this.bs = bs;
        //}
    }

}