﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo
{
    public partial class Exam2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button2_click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(TxtInputFor1_2.Text))
            {
                int inputInt = Int32.Parse(TxtInputFor1_2.Text);
                int size = inputInt;
                string outputStr = "";
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        if (j <= i && (i <= Math.Floor(((double)size) / 2))) // Top-Left (0,0)
                        {
                            outputStr += "X";
                        }
                        else if (j > i && i <= Math.Floor(((double)size) / 2) && j >= (size - 1 - i)) // Top-Right (0,1)
                        {
                            outputStr += "X";
                        }
                        else if (j < i && i >= Math.Floor(((double)size) / 2) && j <= (size - 1 - i)) // Buttom-Left (1,0)
                        {
                            outputStr += "X";
                        }
                        else if (j >= i && (i >= Math.Floor(((double)size) / 2))) // Buttom-Right (1,1)
                        {
                            outputStr += "X";
                        }
                        else
                        {
                            outputStr += "&ensp;";
                        }
                    }
                    if ((i + 1) < size)
                    {
                        outputStr += "<br>";
                    }
                }
                if (inputInt > 0)
                {
                    IntInput2.Text = "n = " + TxtInputFor1_2.Text;
                    Result2.Text = outputStr;
                }
                else
                {
                    IntInput2.Text = "";
                    Result2.Text = "";
                }
            }
            else
            {
                IntInput2.Text = "";
                Result2.Text = "";
            }
        }
    }
}