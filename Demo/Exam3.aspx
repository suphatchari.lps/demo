﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Exam3.aspx.cs" Inherits="Demo.Exam3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Developer Exam 3</title>
    <script src="Scripts/jquery-3.5.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Exam.css" rel="stylesheet" />
</head>

<body>
    <form id="form1" runat="server">
        <div style="font-size: medium;">
            <asp:HyperLink ID="HyperLinkBackFrom3" NavigateUrl="Main.aspx" runat="server"><< Main</asp:HyperLink>
        </div>

        <div style="margin-top:5px">
            <h2>3. Create a page for display and searching users</h2>
        </div>

        <div style="margin-bottom:5px">
            <asp:Label ID="Label2" runat="server" Text="Search : "></asp:Label>
            <asp:TextBox ID="TxtInputFor3" runat="server" TextMode="Search"></asp:TextBox>
            <asp:Button ID="bt33" runat="server" OnClick="Button_Search" Text="Search"/>
            <asp:Button ID="clearBt" runat="server" OnClick="Button_Clear" Text="Clear" style="margin-left:20px"/>
        </div>

        <asp:GridView ID = "UserGridView" runat = "server" autogeneratecolumns="false" >
            <Columns>
                <asp:BoundField DataField="id" HeaderText="ID"/>
                <asp:BoundField DataField="name" HeaderText="Name"/>
                <asp:BoundField DataField="username" HeaderText="Username"/>
                <asp:BoundField DataField="email" HeaderText="Email"/>
                <asp:BoundField DataField="address.street" HeaderText="Address: Street"/>
                <asp:BoundField DataField="address.suite" HeaderText="Address: Suite"/>
                <asp:BoundField DataField="address.city" HeaderText="Address: City"/>
                <asp:BoundField DataField="address.zipcode" HeaderText="Address: Zipcode"/>
                <asp:BoundField DataField="address.geo.lat" HeaderText="Address: Geo: Lat"/>
                <asp:BoundField DataField="address.geo.lng" HeaderText="Address: Geo: Lng"/>
                <asp:BoundField DataField="phone" HeaderText="Phone"/>
                <asp:BoundField DataField="website" HeaderText="Website"/>
                <asp:BoundField DataField="company.name" HeaderText="Company: Name"/>
                <asp:BoundField DataField="company.catchPhrase" HeaderText="Company: Catch Phrase"/>
                <asp:BoundField DataField="company.bs" HeaderText="Company: BS"/>
            </Columns>
            <HeaderStyle HorizontalAlign="Center" Font-Size="Small" BackColor="LightGray"/>
            <RowStyle HorizontalAlign="Left" Font-Size="x-Small"></RowStyle>
        </asp:GridView>

        <div class="row" style="padding: 5px; margin: 5px;">
            <div class="col-sm-12" style="text-align: center; border-style: none; font-size: medium; font-family: Tahoma;">
                <p style="padding: 0px; margin: 0px; margin-bottom:5px">
                    <asp:Label ID="result" runat="server" Text=""></asp:Label>
                </p>
            </div>
        </div>

    </form>
</body>
</html>
